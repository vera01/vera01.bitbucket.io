
let pg;
let myFont;
let mode;
let v1, v2;

let tilesX;
let tilesY;

function preload() {
  myFont = loadFont('assets/Poppins-Black.otf');
}

function setup() {
  createCanvas(windowWidth, windowHeight, P2D);
  background(0);
   pg = createGraphics(windowWidth, windowHeight);
   mode = 0;
   v1 = 35;
   v2 = 35;
   tilesY = 75;
   tilesX = 100;
}

function draw() {
  background(0,0,53);
  // PGraphics
  pg.background(0,0,53);
  pg.fill(255,0,55);
  pg.ellipse(180,350,325,325);
  pg.fill(255,255,50);
  pg.push();
  pg.translate(600, 350);
  myForm(0,0,3,200, -frameCount*0.95);
  pg.translate(400, 0);

  pg.fill(20,20,255);
  myCross(0,0,125, frameCount*1.5);
  pg.pop();

  pg.textFont(myFont);
  pg.push();
  pg.translate(50, 50);
  pg.textAlign(LEFT);
  pg.textSize(190);
  pg.fill(255);
  pg.text("RECODE", 5, 100);
  pg.text("EXPO",5, 230);

  //pg.fill(255);
  pg.text("ANNÉE II",5, 360);
  pg.text("07.05.19",5, 490);

  pg.translate(30, 30);
  pg.textSize(53);
  pg.text("école supérieure", 5, 550);
  pg.text("d'art de Cambrai", 5, 600);
  pg.pop();



  let tileW = int(width/tilesX);
  let tileH = int(height/tilesY);

  for (var y = 0; y < tilesY; y++) {
    for (var x = 0; x < tilesX; x++) {

      // WARP
      let freqX = map(mouseX, 0, width, 0.001, 0.15);
      let freqXX = map(mouseY, 0, height, 0.001, 0.005);
      let amp = map(mouseY, 0, height, 1, 300);
      let waveX = int(sin((y*0.015) + frameCount * freqX + ( x * y ) * freqXX) * v1);
      let waveY = 0;
      let noiseX = int(noise(frameCount * freqX + ( x * y ) * freqXX) * v2);

      if(mode===0){
        v1 = amp;
        v2 = 33;
        tilesY = 75;
      }

      if(mode===1){
        v1 = 0;
        v2 = amp/2;
      }
      if(mode===2){
        v1 = 200;
        v2 = 33;
        tilesY = 15;
      }
      if(mode===3){
        v1 = 900;
        v2 = 33;
      }

      if(mode===4){
        v1 = amp;
        tilesX = 30;
        tilesY = 100;
        waveY = int(sin((y*0.015) + frameCount * freqX + ( x * y ) * freqXX) * v1);
      }


      // SOURCE
      let sx = x*tileW + waveX;
      let sy = y*tileH + waveY;
      let sw = tileW;
      let sh = tileH + noiseX;


      // DESTINATION
      let dx = x*tileW;
      let dy = y*tileH;
      let dw = tileW;
      let dh = tileH;

      copy(pg, sx, sy, sw, sh, dx, dy, dw, dh);

    }
  }

  let link = createDiv("Plus d'info <a href='http://www.esac-cambrai.net/wordpress'target='_blank'>recode</a>");
   link.style("font-family", "Poppins-Black");
   link.style("font-size", "25pt");
   link.style("color", "white");
   link.position(85, windowHeight-80);

   let txt = createDiv("click then move | Poppins font by <a href ='https://github.com/itfoundry/poppins'target='_blank'> Indian Type Foundry</a>");
    txt.style("font-family", "Poppins-Light");
    txt.style("font-size", "11pt");
    txt.style("letter-spacing: ", "125pt");
    txt.style("color", "white");
    txt.position(85, windowHeight-35);

}

function mouseClicked(){
  if(mode<5){
  mode++;
}else{
  mode = 0;
}
//  console.log(mode);
}

function myForm(_cntX, _cntY, _num, _rad, _theta){
  pg.push();
  pg.translate(_cntX, _cntY);
  pg.rotate(radians(_theta));
  var angle = radians(360/_num);
  pg.beginShape();
  for (var i = 0; i<_num; i++) {
   let x = cos(i * angle) * _rad;
   let y = sin(i * angle) * _rad;
   pg.vertex(x, y);
 }
 pg.endShape(CLOSE);
 pg.pop();
}

function myCross(_cntX, _cntY, _size, _theta){
  pg.push();
  pg.noStroke();
  pg.rectMode(CENTER);
  pg.translate(_cntX, _cntY);
  pg.rotate(radians(_theta));
  pg.rect(0, 0,_size*3, _size);
  pg.rect(0, 0,_size, _size*3);
  pg.pop();
}
